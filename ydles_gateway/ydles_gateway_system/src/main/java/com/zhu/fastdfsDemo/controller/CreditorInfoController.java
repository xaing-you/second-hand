package com.zhu.fastdfsDemo.controller;

import com.zhu.fastdfsDemo.entity.CreditorInfo;
import com.zhu.fastdfsDemo.service.CreditorInfoService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * (CreditorInfo)表控制层
 *
 * @author makejava
 * @since 2021-11-28 17:08:25
 */
@RestController
@RequestMapping("creditorInfo")
public class CreditorInfoController {
    /**
     * 服务对象
     */
    @Resource
    private CreditorInfoService creditorInfoService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public CreditorInfo selectOne(Integer id) {
        return this.creditorInfoService.queryById(id);
    }

}
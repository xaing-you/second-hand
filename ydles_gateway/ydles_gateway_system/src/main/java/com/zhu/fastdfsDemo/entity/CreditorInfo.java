package com.zhu.fastdfsDemo.entity;

import java.io.Serializable;

/**
 * (CreditorInfo)实体类
 *
 * @author makejava
 * @since 2021-11-28 17:08:25
 */
public class CreditorInfo implements Serializable {
    private static final long serialVersionUID = -69324583535732994L;
    /**
    * 主键
    */
    private Integer id;
    /**
    * 债权借款人姓名
    */
    private String realname;
    /**
    * 债权借款人身份证
    */
    private String idcard;
    /**
    * 债权借款人地址
    */
    private String address;
    /**
    * 1男2女
    */
    private Integer sex;
    /**
    * 债权借款人电话
    */
    private String phone;
    /**
    * 债权借款人借款金额
    */
    private Double money;
    /**
    * 债权合同所在组
    */
    private String groupname;
    /**
    * 债权合同所在路径
    */
    private String remotefilepath;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname;
    }

    public String getIdcard() {
        return idcard;
    }

    public void setIdcard(String idcard) {
        this.idcard = idcard;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Double getMoney() {
        return money;
    }

    public void setMoney(Double money) {
        this.money = money;
    }

    public String getGroupname() {
        return groupname;
    }

    public void setGroupname(String groupname) {
        this.groupname = groupname;
    }

    public String getRemotefilepath() {
        return remotefilepath;
    }

    public void setRemotefilepath(String remotefilepath) {
        this.remotefilepath = remotefilepath;
    }

}
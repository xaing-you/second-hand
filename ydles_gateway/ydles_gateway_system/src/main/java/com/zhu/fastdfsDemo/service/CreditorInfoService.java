package com.zhu.fastdfsDemo.service;

import com.zhu.fastdfsDemo.entity.CreditorInfo;
import java.util.List;

/**
 * (CreditorInfo)表服务接口
 *
 * @author makejava
 * @since 2021-11-28 17:08:25
 */
public interface CreditorInfoService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    CreditorInfo queryById(Integer id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<CreditorInfo> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param creditorInfo 实例对象
     * @return 实例对象
     */
    CreditorInfo insert(CreditorInfo creditorInfo);

    /**
     * 修改数据
     *
     * @param creditorInfo 实例对象
     * @return 实例对象
     */
    CreditorInfo update(CreditorInfo creditorInfo);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Integer id);

}
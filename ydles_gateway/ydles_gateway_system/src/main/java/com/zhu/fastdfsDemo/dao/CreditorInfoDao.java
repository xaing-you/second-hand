package com.zhu.fastdfsDemo.dao;

import com.zhu.fastdfsDemo.entity.CreditorInfo;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * (CreditorInfo)表数据库访问层
 *
 * @author makejava
 * @since 2021-11-28 17:08:25
 */
public interface CreditorInfoDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    CreditorInfo queryById(Integer id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<CreditorInfo> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param creditorInfo 实例对象
     * @return 对象列表
     */
    List<CreditorInfo> queryAll(CreditorInfo creditorInfo);

    /**
     * 新增数据
     *
     * @param creditorInfo 实例对象
     * @return 影响行数
     */
    int insert(CreditorInfo creditorInfo);

    /**
     * 修改数据
     *
     * @param creditorInfo 实例对象
     * @return 影响行数
     */
    int update(CreditorInfo creditorInfo);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Integer id);

}
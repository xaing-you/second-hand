package com.zhu.fastdfsDemo.service.impl;

import com.zhu.fastdfsDemo.entity.CreditorInfo;
import com.zhu.fastdfsDemo.dao.CreditorInfoDao;
import com.zhu.fastdfsDemo.service.CreditorInfoService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (CreditorInfo)表服务实现类
 *
 * @author makejava
 * @since 2021-11-28 17:08:25
 */
@Service("creditorInfoService")
public class CreditorInfoServiceImpl implements CreditorInfoService {
    @Resource
    private CreditorInfoDao creditorInfoDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public CreditorInfo queryById(Integer id) {
        return this.creditorInfoDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    @Override
    public List<CreditorInfo> queryAllByLimit(int offset, int limit) {
        return this.creditorInfoDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param creditorInfo 实例对象
     * @return 实例对象
     */
    @Override
    public CreditorInfo insert(CreditorInfo creditorInfo) {
        this.creditorInfoDao.insert(creditorInfo);
        return creditorInfo;
    }

    /**
     * 修改数据
     *
     * @param creditorInfo 实例对象
     * @return 实例对象
     */
    @Override
    public CreditorInfo update(CreditorInfo creditorInfo) {
        this.creditorInfoDao.update(creditorInfo);
        return this.queryById(creditorInfo.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return this.creditorInfoDao.deleteById(id) > 0;
    }
}
package Test;

import org.junit.Test;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.Aware;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/*
 *
 * @Author zhu
 * @Create 2021/11/26
 *
 */
@SpringBootTest
public class Demo02 implements ApplicationContextAware,Aware {

    private ApplicationContext applicationContext;

    @Test
    public void demo01(){
        System.out.println(applicationContext);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}

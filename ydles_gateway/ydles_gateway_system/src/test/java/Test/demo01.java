package Test;
/*
 *
 * @Author zhu
 * @Create 2021/11/26
 *
 */

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.junit.Test;
import org.springframework.security.crypto.bcrypt.BCrypt;

import java.util.Date;

public class demo01 {

    @Test
    public void demo01(){
        for (int i = 0; i < 10; i++) {
            String salt = BCrypt.gensalt();
            System.out.println(salt);
            String s = BCrypt.hashpw("1234",salt);
            System.out.println(s);
            System.out.println("=================================");
            System.out.println(BCrypt.checkpw("1234",s));
            System.out.println("=================================");

        }
    }

    @Test
    public void demo02(){
        JwtBuilder builder = Jwts.builder().setId("朱肖阳").setSubject("朱肖阳测试下一jwt").setIssuedAt(new Date())
                .signWith(SignatureAlgorithm.HS256, "zhuxiaoyang");
        String compact = builder.compact();
        System.out.println(compact);
        System.out.println("=======================");
        Claims body = Jwts.parser().setSigningKey("zhuxiaoyang").parseClaimsJws(compact).getBody();
        System.out.println(body);

    }

}

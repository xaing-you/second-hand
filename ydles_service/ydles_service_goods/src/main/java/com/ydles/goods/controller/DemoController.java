package com.ydles.goods.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/*
 *
 * @Author zhu
 * @Create 2021/11/25
 *
 */
@RestController
public class DemoController {

    @GetMapping("hello")
    public String demo01(){

        return "你好";
    }
}

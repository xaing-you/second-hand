package com.ydles.goods.handler;

import com.ydles.entity.Result;
import com.ydles.entity.StatusCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

/*
 * 这个只是一个简单的全局异常处理类，如果需要补充的话可以通过定义统一异常返回体，异常代码等进行相关扩展
 *  声明一个处理全局异常的类。通过AOP实现
 * 不需要在每个方法中进行异常的捕获了，直接同一进行了处理操作，便于代码的整洁和易读
 * @Author zhu
 * @Create 2021/11/25
 *
 */
@Slf4j
@ControllerAdvice  // 声明为一个 controller 层的切面
public class BaseExceptionHandler {

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public Result error(Exception e){
        e.printStackTrace();
        log.error(e.getMessage());
        return new Result(false, StatusCode.ERROR,e.getMessage());
    }
}

package com.ydles.goods.controller;

import com.ydles.entity.Result;
import com.ydles.goods.pojo.Brand;
import com.ydles.goods.service.BrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/*
 *
 * @Author zhu
 * @Create 2021/11/25
 *
 */
@CrossOrigin
@RestController
public class BrandController {

    @Autowired
    BrandService brandService;

    @GetMapping("findAll")
    public Result<List<Brand>> findAll(){
        List<Brand> brands = brandService.findAll();
        Result<List<Brand>> result = new Result<>(true, 20000, "查询成功", brands);
        return result;
    }

    @GetMapping("{id}")
    public Result findBrandByid(@PathVariable("id") Integer id){

        Brand brand = brandService.findBrandById(id);
        Result<Object> result = new Result<>();
        result.setData(brand);
        return result;
    }

    @PostMapping("insert")
    public Result add(@RequestBody Brand brand){
        brand.setSeq((int) Math.random());
        try {
            brandService.add(brand);
            return new Result();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new Result(false,10000,"添加失败");
    }

    @GetMapping("search/{page}/{size}")
    public Result<List<Brand>> search(@PathVariable("page") Integer page,@PathVariable("size") Integer size){
        List<Brand> list = brandService.findPage(page, size);
        Result<List<Brand>> result = new Result(true, 1000,"查询成功", list);
        return result;
    }

}

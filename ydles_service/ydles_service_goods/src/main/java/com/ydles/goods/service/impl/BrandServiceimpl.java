package com.ydles.goods.service.impl;
/*
 *
 * @Author zhu
 * @Create 2021/11/25
 *
 */

import com.github.pagehelper.PageHelper;
import com.ydles.goods.dao.BrandMapper;
import com.ydles.goods.pojo.Brand;
import com.ydles.goods.service.BrandService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class BrandServiceimpl implements BrandService {

    @Autowired
    BrandMapper brandMapper;
    @Override
    public List<Brand> findAll() {
        return brandMapper.selectAll();
    }

    @Override
    public Brand findBrandById(Integer id) {
        Brand brand = brandMapper.selectByPrimaryKey(id);
        return brand;
    }

    @Override
    public void add(Brand brand) {
        int insert = brandMapper.insert(brand);
        if (insert > 0){
            log.info("添加成功");
        }
        log.error("添加失败");
    }

    @Override
    public List<Brand> findPage(Integer page, Integer size) {

        PageHelper.startPage(page,size);
        List<Brand> list = brandMapper.selectAll();
        return list;
    }


}

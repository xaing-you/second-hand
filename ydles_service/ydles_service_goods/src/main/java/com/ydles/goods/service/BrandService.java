package com.ydles.goods.service;
/*
 *
 * @Author zhu
 * @Create 2021/11/25
 *
 */

import com.ydles.goods.pojo.Brand;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.util.List;

public interface BrandService {

    public List<Brand> findAll();

    public Brand findBrandById(Integer id);

    public void add(Brand brand);

    public List<Brand> findPage(Integer page, Integer size);
}

package com.ydles.goods.dao;
/*
 *
 * @Author zhu
 * @Create 2021/11/25
 *
 */

import com.ydles.goods.pojo.Brand;
import tk.mybatis.mapper.common.Mapper;


public interface BrandMapper extends Mapper<Brand> {

}

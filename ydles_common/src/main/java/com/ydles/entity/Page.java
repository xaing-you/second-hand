package com.ydles.entity;
/*
 *
 * @Author zhu
 * @Create 2021/11/25
 *
 */

import java.io.Serializable;
import java.util.List;

public class Page<T> implements Serializable {

    // 传递的参数或配置的项
    private int currentPage; // 当前页
    private int pageSize; // 每页显示多少条

    // 查询数据库
    private int recordCount; // 总记录数
    private List recordList; // 本页的数据列表

    // 计算
    private int pageCount; // 总页数
    private int beginPageIndex; // 页码列表的开始索引
    private int endPageIndex; // 页码列表的结束索引

    /**
     * 只需要接受前4个参数的值，会自动的计算出后3个属性的值。
     *
     * @param currentPage
     * @param pageSize
     * @param recordCount
     * @param recordList
     */
    public Page(int currentPage, int pageSize, int recordCount, List recordList) {
        this.currentPage = currentPage;
        this.pageSize = pageSize;
        this.recordCount = recordCount;
        this.recordList = recordList;

        // 计算pageCount
        pageCount = (recordCount + pageSize - 1) / pageSize;

        // 计算beginPageIndex和endPageIndex
        // 当页码数量不大于10个时，显示所有页码。
        if (pageCount <= 10) {
            beginPageIndex = 1;
            endPageIndex = pageCount;
        }
        // 当页码数量大于10个时，显示当前页附近的共10个页码。
        else {
            // 一般情况下显示前4个加当前页加后5个（共10个）
            beginPageIndex = currentPage - 4;
            endPageIndex = currentPage + 5;

            // 当前面不足4个页码时，显示前10个页码
            if (beginPageIndex < 1) {
                beginPageIndex = 1;
                endPageIndex = 10;
            }
            // 当后面不足5个页码时，显示后10个页码
            else if (endPageIndex > pageCount) {
                endPageIndex = pageCount;
                beginPageIndex = pageCount - 10 + 1; // 显示时会包含两边的边界，所以要减9.
            }
        }

    }
}

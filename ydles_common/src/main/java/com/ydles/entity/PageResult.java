package com.ydles.entity;
/*
 * 分页查询的实体类
 * @Author zhu
 * @Create 2021/11/25
 *
 */

import lombok.Data;

import java.util.List;

@Data
public class PageResult<T> {

    private Long total;//总记录数
    private List<T> rows;//记录
    public PageResult(Long total, List<T> rows) {
        this.total = total;
        this.rows = rows;
    }
    public PageResult() {
    }
}

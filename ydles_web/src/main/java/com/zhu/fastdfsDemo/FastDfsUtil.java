package com.zhu.fastdfsDemo;
/*
 *
 * @Author zhu
 * @Create 2021/11/28
 *
 */

import org.csource.common.MyException;
import org.csource.fastdfs.*;

import java.io.IOException;

public class FastDfsUtil {
    public static void main(String[] args) {
//        upload();
//        download();
        delete();
    }

    /**
     * 上传文件操作
     */
    private static void upload() {
        TrackerServer ts = null;
        StorageServer st = null;
        try {
            ClientGlobal.init("fdfs_client.conf");
            TrackerClient client = new TrackerClient();
            ts = client.getConnection();
            st = client.getStoreStorage(ts);
            StorageClient storageClient = new StorageClient(ts, st);

            String[] phone = storageClient.upload_file("d:/1.jpg", "jpg", null);
            for (String s : phone) {
                System.out.println(s);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (MyException e) {
            e.printStackTrace();
        }finally {
            if (ts != null){
                try {
                    ts.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (st != null){
                try {
                    st.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    /**
     * 下载文件
     */
    private static void download() {
        TrackerServer ts = null;
        StorageServer st = null;
        try {
            ClientGlobal.init("fdfs_client.conf");
            TrackerClient client = new TrackerClient();
            ts = client.getConnection();
            st = client.getStoreStorage(ts);
            StorageClient storageClient = new StorageClient(ts, st);

//            String[] phone = storageClient.upload_file("d:/1.jpg", "jpg", null);

            String file = "M00/00/00/wKjIgGGjr4GAf8kpAAGXNzD5XKU719.jpg";
            int result = storageClient.download_file("group1", file, "C:\\Users\\zhu\\Desktop\\13.jpg");
            System.out.println(result);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (MyException e) {
            e.printStackTrace();
        }finally {
            if (ts != null){
                try {
                    ts.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (st != null){
                try {
                    st.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }


    /**
     * 删除文件
     */
    private static void delete() {
        TrackerServer ts = null;
        StorageServer st = null;
        try {
            ClientGlobal.init("fdfs_client.conf");
            TrackerClient client = new TrackerClient();
            ts = client.getConnection();
            st = client.getStoreStorage(ts);
            StorageClient storageClient = new StorageClient(ts, st);

//            String[] phone = storageClient.upload_file("d:/1.jpg", "jpg", null);
            String file = "M00/00/00/wKjIgGGjr4GAf8kpAAGXNzD5XKU719.jpg";
            int result = storageClient.delete_file("group1", file);
            System.out.println(result);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (MyException e) {
            e.printStackTrace();
        }finally {
            if (ts != null){
                try {
                    ts.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (st != null){
                try {
                    st.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}

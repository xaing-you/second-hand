package com.zhu.fastdfsDemo.controller;

import com.zhu.fastdfsDemo.entity.CreditorInfo;
import com.zhu.fastdfsDemo.service.CreditorInfoService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.List;

/**
 * (CreditorInfo)表控制层
 *
 * @author makejava
 * @since 2021-11-28 17:10:07
 */
@Controller
public class CreditorInfoController {
    /**
     * 服务对象
     */
    @Resource
    private CreditorInfoService creditorInfoService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne/{id}")
    public String selectOne(@PathVariable("id") Integer id) {
         CreditorInfo creditorInfo = creditorInfoService.queryById(id);
         System.out.println(creditorInfo);
         return "OK";
    }

    @GetMapping("findAll")
    public String selectAll(Model model){
        List<CreditorInfo> creditorInfos = creditorInfoService.queryAllByLimit(0,10);
        model.addAttribute("creditorList",creditorInfos);
        return "creditors.html";
    }

    @PostMapping("upload")
    public String upload(MultipartFile file,Model model){

        
        return "";
    }

}
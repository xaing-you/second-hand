package com.zhu.fastdfsDemo;

import org.csource.common.MyException;
import org.csource.fastdfs.*;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.io.IOException;

/*
 *
 * @Author zhu
 * @Create 2021/11/28
 *
 */
@SpringBootApplication
public class FastDfsApplication {
    public static void main(String[] args) {
        SpringApplication.run(FastDfsApplication.class, args);
    }

    @Bean
    public StorageClient getStorgerBean() {

        TrackerServer ts = null;
        StorageServer st = null;
        try {
            ClientGlobal.init("fdfs_client.conf");
            TrackerClient client = new TrackerClient();
            ts = client.getConnection();
            st = client.getStoreStorage(ts);
            StorageClient storageClient = new StorageClient(ts, st);
            return storageClient;

        } catch (IOException e) {
            e.printStackTrace();
        } catch (MyException e) {
            e.printStackTrace();
        } finally {
            if (ts != null) {
                try {
                    ts.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }
}
